#ifndef TRUTHOBJECTLOADER_H_
#define TRUTHOBJECTLOADER_H_

#include "TopAnalysis/ObjectLoaderBase.h"

/**
 *  * A class that can be loaded by name at run time and creates our object selection
 *   */
class TruthObjectLoader : public top::ObjectLoaderBase {
   public:
       //A method that creates a pointer to a TopObjectSelection tool which contains info on which cuts to enable and configure
     top::TopObjectSelection* init(std::shared_ptr<top::TopConfig> topConfig);
       //
       //        //Clever root stuff
     ClassDef(TruthObjectLoader, 0)
};
       //
#endif
