#include "TruthAnalysis/TruthObjectLoader.h"
#include "TruthAnalysis/TruthEventSaver.h"

#ifdef __CINT__
#pragma extra_include "TruthAnalysis/TruthObjectLoader.h";
#pragma extra_include "TruthAnalysis/TruthEventSaver.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

// For loading the object selection at run time
#pragma link C++ class TruthObjectLoader+;
#pragma link C++ class top::TruthEventSaver+;

#endif
